import ballerinax|mongodb;
import Customer.bal;
	
	public class Store{
	private mongodb:Client mongo_alt;
	private string mong_col;

	public init(model;DBConfig config) returns error{
	mongodb:ClientConfig mongo_config = {
	port: config.port,	
	host: config.host
	};
	self.mongo_alt = check new (mongo_config, config.dbname);
	self.mongo_col = config.collection;
	}

  public type DBconfig record{|
  string host;
  string port;
  string dbname;
  string collection;
  |};

  public function get_materials(int? Dish-soap, int? Cereal, int? Shampoo, int? Lotion, int? Rice, int? Macoroni, int? Chicken, int? Cheese, int? Amount-Ordered, int? Amount-delivered) returns model:material[]|error{
map(json) search_query = {};
if(Dish-soap != null){
seacrh_query["Dish-soap"] = Dish-soap;
}
if(Cereal != null){
search_query["Cereal"] = Cereal;
}
if(Shampoo != ()){
search_query["Shampoo"] = Shampoo;
}
if(Lotion != ()){
search_query["Lotion"] = Lotion;
}
if(Rice != ()){
search_query["Rice"] = Rice;
}
if(Macoroni != ()){
search_query["Macoroni"] = Macoroni;
}
if(Chicken != ()){
search_query["Chicken"] = Chicken;
}
if(Cheese != ()){
search_query["Cheese"] = Cheese;
}
if(Amount-Ordered != ()){
search_query["Amount-Ordered"] = Amount-Ordered;
}
if(Amount-delivered != ()){
search_query["Amount-delivered"] = Amount-delivered;
}
map(json)[] the_res = check self.mongo_alt ->find(self.mongo_col, (), searchquery);
model:material all_mat = [];
foreach var single_mat in the_res{
model:material|error the_mat = single_mat.fromJsonWithType(model:material);
if(the_mat is model:material){
all_mats.push(the_mat);
}
}
return all_mat;
}