import ballerina/http;
public type Customer record{|
string Firstname;
string Lastname;
readonly int customerNumber;
|};

table <Customer> key (customerNumber) CustomerTable = table [
    {customerNumber: 24680, Firstname: "John", Lastname: "Dahmer"}
];

service/CustomerService on new http:Listener(9090){
    resource function get all() returns Customer[]{
        return CustomerTable.toArray();
    }
}