import ballerina/http;
import ballerina/io;

// curl http://localhost:9090/CustomerService/all
public function main() returns error? {
    http:Client checkClient = check new http:Client("http://localhost:9090/CustomerService");
    json all_Customers = check  checkClient -> get ("/all");
    io:println(all_Customers);
}
